# Hur

Hur means free, project to allow freelancers to hold their profile and services in their own repository and the website will collect and list them.
This section handles the conversation of `toml` files to `MarkDown` files representing user profile and services.


## Requirements

* `tomli`
* `snakemd`

## Screenshot

![Peek_2023-07-16_09-00](/uploads/6196dde5106cbec42f9e5da722bf0a1e/Peek_2023-07-16_09-00.mp4)

## Usage
Using one of the templates in `toml` directory create your own and then run the `convert_toml2md.py` script. Markdown files will be generated.

## Support
Please open an issue if you need support.

## License

- AGPL v3
